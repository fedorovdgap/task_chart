

import Rectangle from './canvas/Rectangle.js'

export default class Tooltip{
    constructor(ctx, position, value, options) {
        this.ctx = ctx
        this.position = position
        this.value = value
        this.options = {
            textAlign: 'center', //
            textColor: '#ffffff', //
            fontSize: 9,
            backgroundColor: '#',
            backgroundBorderRadius: 8,
            padding: [16,12]
        }
    }
    draw() {
        this.drawBackground() //Рисуем фон
        this.drawText() //Рисуем текст
    }
    getTextWidth(){
        return this.ctx.measureText(this.value).width
    }
    drawText(){
        this.ctx.fillStyle = "#fff";
        this.ctx.font = "12px Arial";
        this.ctx.fillText(this.value, this.position.x - this.getTextWidth()/2, this.position.y + 5 + this.options.fontSize + this.options.padding[1]);
    }
    drawBackground(){
        this.ctx.fillStyle = '#131313'
        Rectangle(this.ctx, this.position.x -(this.getTextWidth()/2 + this.options.padding[0]), this.position.y+5, this.getTextWidth() + this.options.padding[0]*2, this.options.fontSize + this.options.padding[1]*2, this.options.backgroundBorderRadius, true, false)
    }
}