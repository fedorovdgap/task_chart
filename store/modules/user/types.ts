interface IChartDataItem {
  x: IChartDataElement;
  y: IChartDataElement;
}

interface IChartDataElement {
  value: any;
  displayName: string;
}

export interface Dataset extends Array<IChartDataItem>{}
