import { GetterTree } from 'vuex'
import { Dataset } from "~/store/modules/user/types"
import { RootState } from "~/store/types"

export const getters: GetterTree<Dataset,RootState> = {
  getDataset (state: Dataset): Dataset {
    return state
  }
}
