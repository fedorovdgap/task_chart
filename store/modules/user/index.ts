import { Module } from 'vuex'
import { RootState } from "~/store/types"
import { Dataset } from "~/store/modules/user/types"
import { getters } from "~/store/modules/user/getters"
import { mutations} from "~/store/modules/user/mutations"

const state: Dataset = [
  {
    y: {
      value: '17',
      displayName: '15'
    },
    x:{
      value: '5.10.2022',
      displayName: 'Oct 5'
    }
  },
  {
    y: {
      value: 7,
      displayName: '5,000,000'
    },
    x:{
      value: '6.10.2022',
      displayName: 'Oct 6'
    }
  },
  {
    y: {
      value: 8,
      displayName: '5,000,000'
    },
    x:{
      value: '7.10.2022',
      displayName: 'Oct 7'
    }
  },
  {
    y: {
      value: 11,
      displayName: '5,000,000'
    },
    x:{
      value: '8.10.2022',
      displayName: 'Oct 8'
    }
  },
  {
    y: {
      value: 27,
      displayName: '5,000,000'
    },
    x:{
      value: '9.10.2022',
      displayName: 'Oct 9'
    }
  },
  {
    y: {
      value: 13,
      displayName: '5,000,000'
    },
    x:{
      value: '11.10.2022',
      displayName: 'Oct 10'
    },
  },
  {
    y: {
      value: 13,
      displayName: '15'
    },
    x: {
      value: '11.10.2022',
      displayName: 'oct 11'
    }
  }
]

export const user: Module<Dataset,RootState> = {
  state,
  getters,
  mutations
}
