import { Dataset } from "~/store/modules/user/types"
import { MutationTree } from 'vuex'

export enum DatasetMutations {
  SET_DATASET = "SET_DATASET"
}

export const mutations: MutationTree<Dataset> = {
  [DatasetMutations.SET_DATASET] (state: Dataset, payload: Dataset): void {
    state = payload
  }
}
